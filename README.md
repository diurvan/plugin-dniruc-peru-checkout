# plugin dniruc peru checkout

Plugin para consulta DNI/RUC en el Checkout de WooCommerce para Perú.
Repositorio privado https://gitlab.com/diurvan/wc-dniruc-peru-checkout

wc-dniruc-peru-checkout
Consultar DNI y RUC desde APIs en
https://apiperu.dev/api/
https://diurvanconsultores.com/diurvan_api_dniruc/api
Modificado
Actualizado al 22/01/2021

Privado para consultar DNI y RUC desde APIs en
<!--https://api.sunat.cloud/ruc/
https://api.reniec.cloud/dni/
-->
https://apiperu.dev/api/
https://diurvanconsultores.com/diurvan_api_dniruc/api
Modificado

Actualizado al 19/08/2020
* VERSIONES MINIMAS PARA PLUGIN
* WooCommerce: 4.9.0
* Version PHP: 7.3
* Version WordPress: 5.5

Cambios:

1. Se quita las funciones Localize que ya no se admite en WP 5.5. Se reemplaza llevando las funciones js a archivos JS
2. Se añade el servicio api de consulta de diurvan https://diurvanconsultores.com/diurvan_api_dniruc/api


Puede consultarnos en https://wa.me/+51955478664 o a ivan.tapia@diurvanconsultores.com
